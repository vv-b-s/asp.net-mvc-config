# How to deploy

* .NET Core applications should work on any platform.
* To ensure the application will work on your computer please consider you have the following:
    * [.NET Core SDK](http://dot.net) (Version 2.1)
    * Microsoft SQL Server - either localdb or [full version](https://www.microsoft.com/en-us/sql-server/sql-server-2017)
    
* On your database server create an empty database.
* Edit the connection string in `appsettings.json` to point to your database.
* Execute the following commands in your terminal:

```
#Download all Nuget dependencies
dotnet restore

#Create tables in the database
dotnet ef database update

#Deploy the application
dotnet run
```

* The application should start listening on `http://localhost:5000`
* To terminate the server press `ctrl+C`

> WARN: The application will try redirect to secure connection `https://localhost:5001`, but since there is no certificate, some browsers might block your connection. 


# ASP.NET Core MVC Application deployment and configuration

### Configuring the application to work with Microsoft SQL Server

To start off, make the application use MSSQL database server by importing some of the Entity Framework Core Nuget packages:

```
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
dotnet add package Microsoft.EntityFrameworkCore.Tools
```

After the import is complete check if the `appsettings.json` file looks like this:
```json

{
  "ConnectionStrings": {
    "Context": "Server=localhost,1433;Database=TestEF;User=sa;Password=Adm!n!strAt0r;"
  },
  
  "Logging": {
    "LogLevel": {
      "Default": "Warning"
    }
  },
  "AllowedHosts": "*"
}

```

* What's important here is the `ConnectionStrings` variable which holds the connection string.
* The `"Context"` key corresponds to the `Context.cs` class. Name this variable respectively.
* The value points to the MSSQL Server instance which for the example is on `localhost:1433`.

Create your database context. The initial class should look like this:
```C#
using Microsoft.EntityFrameworkCore;

namespace DemoNetCore.Models
{
    public class Context : DbContext
    {
        public Context(DbContextOptions options) : base(options)
        {
        }
        
        public DbSet<SomeModel> SomeModelSet { get; set; }
    }
}
```

> The parameter `SomeModel` represents the models which will be created during development.

After that go to `Startup.cs` and add the following line in the `ConfigureServices` method:
```C#
        public void ConfigureServices(IServiceCollection services)
        {
            ...
            services.AddDbContext<Context>(o => o.UseSqlServer(Configuration.GetConnectionString("Context")));
        }

```

When the models are created and their DbSets are added to the DbContext class execute the following commands:
```
dotnet ef migrations add InitialCreate
dotnet ef database update
```

* These commands will create the `Migrations` configuration and will write the tables into the database.

> NOTE: To use the database context in your controller you can inject it by adding a constructor:
```C#
    public class HomeController : Controller
    {
        ...
       private Context context;

        public HomeController(Context context)
        {
            this.context = context;
        }
        ...
    }
```

> * More on configuring Microsoft SQL Server with ASP.NET Core application with EF Core on Linux you can find [here](https://docs.docker.com/compose/aspnet-mssql-compose/).
> * More on creating ASP.NET Core MVC application you can find [here](https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-mvc-app-xplat/start-mvc?view=aspnetcore-2.1).

### Scaffolding

To perform scaffolding you need to add ` Microsoft.VisualStudio.Web.CodeGeneration.Design `:
```
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design 
```

After the package is installed to generate the View and Controller of a model use the following command:
```
dotnet aspnet-codegenerator controller -name YourModelController -m YourModel -dc YourContext --relativeFolderPath Controllers --useDefaultLayout --referenceScriptLibraries 
```
