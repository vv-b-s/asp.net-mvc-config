namespace DemoNetCore.Models
{
    public class Human
    {
        public Human()
        {
        }

        public Human(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public long HumanId { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
    }
}