using Microsoft.EntityFrameworkCore;

namespace DemoNetCore.Models
{
    public class Context : DbContext
    {
        public Context(DbContextOptions options) : base(options)
        {
        }
        
        public DbSet<Human> HumanSet { get; set; }
    }
}