using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DemoNetCore.Models;

namespace DemoNetCore.Controllers
{
    public class HumanController : Controller
    {
        private readonly Context _context;

        public HumanController(Context context)
        {
            _context = context;
        }

        // GET: Human
        public async Task<IActionResult> Index()
        {
            return View(await _context.HumanSet.ToListAsync());
        }

        // GET: Human/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var human = await _context.HumanSet
                .FirstOrDefaultAsync(m => m.HumanId == id);
            if (human == null)
            {
                return NotFound();
            }

            return View(human);
        }

        // GET: Human/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Human/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("HumanId,Name,Age")] Human human)
        {
            if (ModelState.IsValid)
            {
                _context.Add(human);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(human);
        }

        // GET: Human/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var human = await _context.HumanSet.FindAsync(id);
            if (human == null)
            {
                return NotFound();
            }
            return View(human);
        }

        // POST: Human/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("HumanId,Name,Age")] Human human)
        {
            if (id != human.HumanId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(human);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HumanExists(human.HumanId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(human);
        }

        // GET: Human/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var human = await _context.HumanSet
                .FirstOrDefaultAsync(m => m.HumanId == id);
            if (human == null)
            {
                return NotFound();
            }

            return View(human);
        }

        // POST: Human/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var human = await _context.HumanSet.FindAsync(id);
            _context.HumanSet.Remove(human);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HumanExists(long id)
        {
            return _context.HumanSet.Any(e => e.HumanId == id);
        }
    }
}
